package com.sapient.zomato.model;

public class RestaurantBean {
public String name,locality,photoUrl,city;

public RestaurantBean(String name, String locality, String photoUrl,String city) {
	super();
	this.name = name;
	this.locality = locality;
	this.photoUrl = photoUrl;
	this.city=city;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getLocality() {
	return locality;
}

public void setLocality(String locality) {
	this.locality = locality;
}

public String getPhotoUrl() {
	return photoUrl;
}

public void setPhotoUrl(String photoUrl) {
	this.photoUrl = photoUrl;
}
}
